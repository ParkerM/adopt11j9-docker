FROM phusion/baseimage:master

ENV JAVA_HOME=/jdk
ENV PATH="/jdk/bin:${PATH}"

ARG BUILD_TYPE="releases"
ARG JDK_BINARY_URL="https://api.adoptopenjdk.net/v2/binary/${BUILD_TYPE}/openjdk11?openjdk_impl=openj9&os=linux&arch=x64&heap_size=normal&release=latest&type=jdk"

RUN curl -L "${JDK_BINARY_URL}" -o openjdk.tar.gz && \
    tar -xzf openjdk.tar.gz && \
    rm -rf openjdk.tar.gz && \
    mv /jdk* /jdk && \
    cd /jdk && \
    java -XshowSettings:properties -version
